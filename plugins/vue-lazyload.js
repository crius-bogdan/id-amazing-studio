import Vue from "vue";
import VueLazyload from "vue-lazyload";

export default async (context, inject) => {
  Vue.use(VueLazyload, {
    preLoad: 3,
    error: require(`${"~/assets/images/loading-image-error.jpg"}`),
    loading: require(`${"~/assets/images/loading-image.svg"}`),
    attempt: 3,
    lazyComponent: true,
    observer: true,
    throttleWait: 500
  });
};
