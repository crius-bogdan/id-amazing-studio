import Vue from "vue";
import SmoothScrollbar from "vue-smooth-scrollbar";

Vue.use(SmoothScrollbar, {
  damping: 0.05,
  thumbMinSize: 100,
  alwaysShowTracks: true
});
