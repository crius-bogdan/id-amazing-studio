export default {
  head() {
    const title = this.content.seo_title || 'I need seo title';
    const meta = this.content.meta || [];

    return {
      title: title,
      meta: [...meta]
    }
  }
}
