import {mapFields} from "vuex-map-fields";
import {mapActions} from "vuex";

export default {
  props: {
    fields_namespace: String
  },
  data() {
    return {
      validation_error: {},
      items: ["Facebook Messenger", "WhatsApp", "Telegram"],
    }
  },
  computed: {
    ...mapFields('contact', [
      'status',
      'name',
      'company',
      'email',
      'phone',
      'messenger_name',
      'messenger_id',
      'message_text'
    ]),
    ...mapFields('page', [
      'user_data',
    ]),
    status_text() {
      const status_answer = this.status === 'error';
      return {
        title: status_answer ? 'Something went wrong ....' : 'Thank you for your appeal.',
        text: status_answer ? 'From pre-seed to series, we accelerate the companies we believe in by generating serious brand equity, product traction, and market-penetrating strategies.' : 'From pre-seed to series, we accelerate the companies we believe in by generating serious brand equity, product traction, and market-penetrating strategies.'
      }
    },
    is_valid() {
      return Object.values(this.validation_error).find(e => !!e);
    },
    messenger: {
      get() {
        return {
          id: this.messenger_id,
          name: this.messenger_name
        }
      },
      set({ id, name }) {
        this.$store.commit('contact/updateField', { path: 'messenger_id', value: id })
        this.$store.commit('contact/updateField', { path: 'messenger_name', value: name })
      }
    }
  },
  methods: {
    sender() {
      const SendingData = {
        'Лид с сайта (Контактная форма)': '%0A',
        'Время (у юзера)': new Date(),
        'Имя': this.name,
        'Компания': this.company,
        'Email': this.email,
        'Телефон': this.phone,
        'Месенжер': this.messenger_name,
        'Юзернейм в месенжере': this.messenger_id,
        'Текст сообщения': this.message_text,
        '----------------': '%0A%0A',
        city: this.user_data.city,
        region: this.user_data.region,
        country: this.user_data.country,
      }

      let SendingString = '------------------------------%0A';
      for(let key in SendingData) {
        SendingString += `<b>${key}</b>: ${SendingData[key]}%0A`;
      }
      SendingString += '------------------------------';
      this.send(SendingString)
    },
    ...mapActions('contact', [
      'send'
    ]),
  },
}
