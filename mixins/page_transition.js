export default {
  transition: {
    beforeLeave() {
      if(process.client) {
        const navigation = document.querySelector(".c-navigation");
        navigation.classList.add("hide");
        navigation.classList.remove("enter");
      }
    },
    enter() {
      if(process.client) {
        const navigation = document.querySelector(".c-navigation");
        navigation.classList.add("enter");
      }
    }
  },
}
