export default {
  props: {
    inputClass: String,
    placeholder: String,
    value: [String, Number],
    input_name: String,
    type: String,
    check_empty: Boolean,
    check_email: Boolean,
    check_name: Boolean,
    check_month: Boolean,
    is_quiz: Boolean,
    quiz_name: String,
    only_numbers: Boolean,
    check_year: Boolean,
    check_day: Boolean,
    check_born_month: Boolean,
    is_address: Boolean,
    input_id: String,
    is_drop_down: Boolean,
  },
  data() {
    return {
      val: this.value,
      id: null,
      is_focus: false,
      is_error: false,
      show_validation: false,
      default_mask: '+1 (___)___-____',
      is_open: false,
    };
  },
  computed: {
    error() {
      if (!this.val || this.val.length === 0) return 'empty';

      return false;
    },
    visible_error() {
      return this.show_validation ? this.error : false;
    },
  },
  created() {
    this.emit_validation();
  },
  methods: {
    change() {
        this.$emit('input', this.val);
        this.emit_validation();
    },
    blur_input() {
      this.is_focus = false;
      this.show_validation = true;
    },
    focus_input() {
      this.is_focus = true;
    },
    emit_validation() {
      this.$emit('update:validation_error', this.error);
    },
    phone_mask() {
      let matrix = this.default_mask,
        i = 0,
        def = matrix.replace(/\D/g, ""),
        val = this.val.replace(/\D/g, "");
      def.length >= val.length && (val = def);

      matrix = matrix.replace(/[_\d]/g, (a) => {
        return val.charAt(i++) || "_"
      });
      this.val = matrix;
      i = matrix.lastIndexOf(val.substr(-1));
      i < matrix.length && matrix !== this.default_mask ? i++ : i = matrix.indexOf("_");
      this.$nextTick(() => this.set_cursor_position(i));
      this.emit_validation();
    },
    set_cursor_position(pos) {
      const elem = this.$refs.input;
      elem.focus()
      elem.setSelectionRange(pos, pos)
    }
  },
}
