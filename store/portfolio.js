import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';
const HOST = process.env.NUXT_ENV_API_HOST;
const STATE = immutableMap({
  date: Date.now(),
  cases: null,
  one_case: null,
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
  clean_case(state) {
    state.case = null
  }
};

export const actions = {
  async load({ commit }) {
    console.log('lol')
    try {
      // https://ipinfo.io/?token=8bd4b15ef11f54
      const data = await this.$axios.$get(HOST + 'portfolio-cases');
      commit('updateField', {path: `cases`, value: data});
    } catch (error) {
    }
    // commit('loading/setStatus', {name: 'page', value: false}, {root: true});
  },
  async load_case({ commit }, id) {
    try {
      const data = await this.$axios.$get(HOST + 'portfolio-cases/' + id);
      commit('updateField', {path: `one_case`, value: data});
    } catch (e) {
    }
  }
};
