import { Map as immutableMap } from "immutable";
import { getField, updateField } from "vuex-map-fields";

const HOST = process.env.NUXT_ENV_API_HOST;

const STATE = immutableMap({
  date: Date.now(),
  user_data: null,
  pages: {}
});

export const state = () => STATE.toJS();

export const getters = {
  getField
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now()
    });
  }
};

const structured_home_data = obj => {
  return {
    seo_title: obj.seo_title,
    meta: obj.meta.map(m => {
      return {
        hid: m.name,
        name: m.name,
        value: m.value
      };
    }),
    title: obj.title,
    description: obj.subtitle,
    about: {
      title: obj.about_block_title,
      about_cards: obj.about_block.map(b => {
        return {
          id: b.id,
          title: b.title,
          description: b.short_description,
          cover: "/api/" + b.cover_image.url
        };
      })
    },
    awards: {
      title: obj.awards_title,
      description: obj.awards_subtitle,
      cards: obj.awards.map(a => {
        return {
          id: a.id,
          title: a.title,
          description: a.description,
          icon: "/api/" + a.icon.url
        };
      })
    },
    cases: {
      title: obj.cases_title,
      description: obj.cases_description,
      case_cards: obj.portfolio_cases.map(c => {
        return {
          cover: "/api/" + c.cover_image.url,
          id: c.id,
          title: c.title,
          description: c.short_description
        };
      })
    },
    worked_width: {
      title: obj.worked_with_title,
      description: obj.worked_with_decription,
      logos: obj.worked_withs.map(w => {
        return {
          id: w.id,
          img: "/api/" + w.logos.url
        };
      })
    },
    internal_cases: {
      title: obj.internal_title,
      description: obj.internal_description,
      cases: obj.internal_cases.map(el => {
        return {
          id: el.id,
          title: el.title,
          description: el.short_description,
          cover: "/api/" + el.cover.url
        };
      })
    },
    blog: {
      title: obj.articles_title,
      description: obj.articles_description,
      posts: obj.articles.map(el => {
        return {
          id: el.id,
          title: el.title,
          description: el.short_description,
          cover: "/api/" + el.cover_image.url
        };
      })
    }
  };
};
const structured_base_page_data = obj => {
  return {
    title: obj.title,
    seo_title: obj.seo_title,
    meta: obj.meta.map(m => {
      return {
        hid: m.name,
        name: m.name,
        value: m.value
      };
    }),
    description: obj.description
  };
};
const structured_brief_data = obj => {
  return {
    title: obj.title,
    seo_title: obj.seo_title,
    meta: obj.meta.map(m => {
      return {
        hid: m.name,
        name: m.name,
        value: m.value
      };
    }),
    description: obj.short_description,
    price_range: obj.price_range,
    product_types: obj.product_types
  };
};
const structured_partners_data = obj => {
  return {
    title: obj.title,
    seo_title: obj.seo_title,
    description: obj.description,
    partners_about_title: obj.partners_about_title,
    partners_about_block: obj.partners_about_block.map(b => {
      return {
        id: b.id,
        title: b.title,
        description: b.short_description,
        cover: "/api/" + b.cover_image.url
      };
    }),
    investors_about_title: obj.investors_about_title,
    investors_about_block: obj.investors_about_block.map(b => {
      return {
        id: b.id,
        title: b.title,
        description: b.short_description,
        cover: "/api/" + b.cover_image.url
      };
    }),
    services_title: obj.services_title,
    services: obj.services.map(el => {
      return {
        id: el.id,
        title: el.title,
        description: el.description,
        slug: el.slug
      };
    })
  };
};
const structured_about_data = obj => {
  return {
    title: obj.title,
    seo_title: obj.seo_title,
    meta: obj.meta.map(m => {
      return {
        hid: m.name,
        name: m.name,
        value: m.value
      };
    }),
    description: obj.description,
    video: "/api/" + obj.video.url,
    video_poster: "/api/" + obj.video_poster.url,
    about: {
      title: obj.about_block_title,
      cards: obj.about_block.map(el => {
        return {
          id: el.id,
          title: el.title,
          description: el.short_description,
          cover: el.cover_image ? "/api/" + el.cover_image.url : null
        };
      })
    },
    awards: {
      title: obj.awards_title,
      description: obj.awards_description,
      cards: obj.awards.map(a => {
        return {
          id: a.id,
          title: a.title,
          description: a.description,
          icon: "/api/" + a.icon.url
        };
      })
    },
    internal_cases: {
      title: obj.cases_title,
      description: obj.cases_description,
      cases: obj.internal_cases.map(el => {
        return {
          id: el.id,
          title: el.title,
          description: el.short_description,
          cover: "/api/" + el.cover.url
        };
      })
    },
    services: obj.services.map(el => {
      return {
        id: el.id,
        title: el.title,
        description: el.description,
        slug: el.slug
      };
    }),
    worked_width: {
      title: obj.worked_with_title,
      description: obj.worked_with_description,
      logos: obj.worked_withs.map(w => {
        return {
          id: w.id,
          img: "/api/" + w.logos.url
        };
      })
    }
  };
};
const structured_service_data = (el, more) => {
  return {
    id: el.id,
    meta: el.meta.map(m => {
      return {
        hid: m.name,
        name: m.name,
        value: m.value
      };
    }),
    title: el.title,
    seo_title: el.seo_title,
    cases_title: el.cases_title,
    cases_description: el.cases_description,
    portfolio: el.portfolio_cases.map(c => {
      return {
        cover: "/api/" + c.cover_image.url,
        id: c.id,
        title: c.title,
        description: c.short_description
      };
    }),
    content_block: el.service_explaining.map(s => {
      return {
        id: s.id,
        has_image: s.has_image,
        cover: s.image ? "/api/" + s.image.url : null,
        title: s.title,
        description: s.description
      };
    }),
    more_services:
      more.length > 0
        ? more.map(el => {
            return {
              id: el.id,
              title: el.title,
              description: el.description,
              slug: el.slug
            };
          })
        : []
  };
};

export const actions = {
  async load({ commit }, page) {
    commit("updateField", { path: "page", value: page });
    try {
      let commit_data = null;
      // https://ipinfo.io/?token=8bd4b15ef11f54
      console.log("эстраница");
      const ip_data = await this.$axios.$get(
        "https://ipinfo.io/?token=8bd4b15ef11f54"
      );
      const data = await this.$axios.$get(HOST + page);
      console.log(data);
      if (page === "home") commit_data = structured_home_data(data);
      if (
        page === "internal-case-page" ||
        page === "blog-page" ||
        page === "portfolio-page" ||
        page === "job-page"
      )
        commit_data = structured_base_page_data(data);
      if (page === "brief") commit_data = structured_brief_data(data);
      if (page === "about") commit_data = structured_about_data(data);
      if (page === "for-partners") commit_data = structured_partners_data(data);

      commit("updateField", { path: `pages.${page}`, value: commit_data });
      commit("updateField", { path: `user_data`, value: ip_data });
    } catch (error) {}
    // commit('loading/setStatus', {name: 'page', value: false}, {root: true});
  },
  async load_service({ commit }, slug) {
    try {
      const ip_data = await this.$axios.$get(
        "https://ipinfo.io/?token=8bd4b15ef11f54"
      );
      const data = await this.$axios.$get(HOST + "services/?slug=" + slug);
      const more_services = await this.$axios.$get(
        HOST + "services/?slug_ne=" + slug
      );
      // console.log(data[0])
      let commit_data = structured_service_data(data[0], more_services);
      // console.log(commit_data)
      commit("updateField", { path: `pages.service`, value: commit_data });
      commit("updateField", { path: `user_data`, value: ip_data });
    } catch (e) {
      console.log(e);
    }
  }
};
