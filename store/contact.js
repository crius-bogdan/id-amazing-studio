import { Map as immutableMap } from "immutable";
import { getField, updateField } from "vuex-map-fields";

const STATE = immutableMap({
  date: Date.now(),
  status: null,
  name: null,
  company: null,
  email: null,
  phone: null,
  messenger_name: null,
  messenger_id: null,
  message_text: null
});

export const state = () => STATE.toJS();

export const getters = {
  getField
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now()
    });
  }
};

export const actions = {
  async send({ commit }, data) {
    try {
      await this.$axios.post(
        "https://api.telegram.org/bot1183330758:AAE_dDxfD0rJW5gkyzuW1Og1Favbybv2uT0/sendMessage?chat_id=-426794697&parse_mode=html&text=" +
          data
      );
      commit("updateField", { path: "status", value: "success" });
    } catch (e) {
      commit("updateField", { path: "status", value: "error" });
    }
  }
};
