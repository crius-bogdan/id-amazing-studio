import { Map as immutableMap } from "immutable";
import { getField, updateField } from "vuex-map-fields";
const HOST = process.env.NUXT_ENV_API_HOST;

const STATE = immutableMap({
  date: Date.now(),
  jobs: [],
  job: null
});

export const state = () => STATE.toJS();

export const getters = {
  getField
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now()
    });
  }
};

export const actions = {
  async load(context) {
    try {
      const data = await this.$axios.$get(HOST + "jobs");
      context.commit("updateField", { path: "jobs", value: data.reverse() });
    } catch (error) {}
    // context.commit('loading/setStatus', {name: 'blog', value: false}, {root: true});
  },
  async load_job_posting({ commit, dispatch }, id) {
    try {
      const data = await this.$axios.$get(HOST + "jobs/" + id);
      commit("updateField", { path: "job", value: data });
      dispatch("overlay/set_overlay", "JobPost", { root: true });
    } catch (e) {}
  },
  async load_job_page_posting({ commit, dispatch }, id) {
    try {
      const data = await this.$axios.$get(HOST + "jobs/" + id);
      commit("updateField", { path: "job", value: data });
    } catch (e) {}
  },
  clear_article({ commit }) {
    commit("updateField", { path: "job", value: null });
  }
};
