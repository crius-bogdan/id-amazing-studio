import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';
import qs from 'qs';
const HOST = process.env.NUXT_ENV_API_HOST;

const STATE = immutableMap({
  date: Date.now(),
  categories: [],
  posts: [],
  article: null
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

export const actions = {
  async load(context) {
    // const query = qs.stringify({ _where: { 'categories.name': 'Features' } })
    try {
      const data = await this.$axios.$get(HOST + 'blogs');
      // const data_test = await this.$axios.$get('/api/blogs?' + query);
      const categories_data = await this.$axios.$get(HOST + 'blog-categories');
      context.commit('updateField', {path: 'categories', value: categories_data});
      context.commit('updateField', {path: 'posts', value: data.reverse()});
    } catch (error) {
    }
    // context.commit('loading/setStatus', {name: 'blog', value: false}, {root: true});
  },
  async get_pasts_by_category({ commit }, category_name) {
    const query = qs.stringify({ _where: { 'categories.name': category_name } })

    try {
      const data = await this.$axios.$get(HOST + 'blogs?' + query);
      commit('updateField', {path: 'posts', value: data.reverse()});
    } catch (e) {

    }
  },
  async load_posts({ commit }) {
    try {
      const data = await this.$axios.$get(HOST + 'blogs');
      commit('updateField', {path: 'posts', value: data.reverse()});
    } catch (e) {

    }
  },
  async load_article({ commit, dispatch }, id) {
    try {
      const data = await this.$axios.$get(HOST + 'blogs/' + id);
      commit('updateField', {path: 'article', value: data});
      dispatch('overlay/set_overlay', 'BlogPost', { root: true })
    } catch (error) {
    }
  },
  async load_page_article({ commit, dispatch }, id) {
    try {
      const data = await this.$axios.$get(HOST + 'blogs/' + id);
      commit('updateField', {path: 'article', value: data});
    } catch (error) {
    }
  },
  clear_article({ commit }) {
    commit('updateField', {path: 'article', value: null});
  }
};
