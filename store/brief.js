import { Map as immutableMap } from "immutable";
import { getField, updateField } from "vuex-map-fields";

const STATE = immutableMap({
  date: Date.now(),
  status: null,
  current_block: "products",
  product_types: [],
  price: null
});

export const state = () => STATE.toJS();

export const getters = {
  getField
};

export const mutations = {
  updateField,
  change_current_block(state, name) {
    state.current_block = name;
  },
  set_product_type(state, type) {
    state.product_types.push(type);
  },
  remove_product_type(state, type) {
    const oldState = [...state.product_types];
    const index = oldState.findIndex(el => el.id === type.id);
    state.product_types.splice(index, 1);
  },
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now()
    });
  }
};

export const actions = {
  async send({ commit }, data) {
    try {
      await this.$axios.post(
        "https://api.telegram.org/bot1183330758:AAE_dDxfD0rJW5gkyzuW1Og1Favbybv2uT0/sendMessage?chat_id=-426794697&parse_mode=html&text=" +
          data
      );
      commit("updateField", { path: "status", value: "success" });
    } catch (e) {
      commit("updateField", { path: "status", value: "error" });
    }
  }
};
