import "./env-config";
export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: "universal",
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "server",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    __dangerouslyDisableSanitizers: ["script"],
    script: [
      {
        src: "https://www.googletagmanager.com/gtag/js?id=UA-176801133-1",
        defer: true
      },
      {
        innerHTML: `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-176801133-1');
        `
      },
      {
        "data-cfasync": "false",
        type: "text/javascript",
        innerHTML: `
        window.civchat = {
          apiKey: "d5zJXs",
        };
        `
      },
      {
        "data-cfasync": "false",
        type: "text/javascript",
        src: "https://interactivedesign.user.com/widget.js"
      }
    ],
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      },
      { name: "msapplication-TileColor", content: "#2d89ef" },
      { name: "theme-color", content: "#ffffff" }
    ],
    link: [
      {
        rel: "apple-touch-icon",
        sizes: "120x120",
        href: "/apple-touch-icon.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: "/favicon-32x32.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: "/favicon-16x16.png"
      },
      { rel: "manifest", href: "/site.webmanifest" },
      { rel: "mask-icon", href: "/safari-pinned-tab.svg", color: "#000000" },
      { rel: "shortcut icon", href: "/icon.png" }
    ]
  },

  loading: { color: "#2C2E35", height: "4px" },
  /*
   ** Global CSS
   */
  css: ["~assets/scss/main.scss"],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    { src: "~/plugins/vue-lazyload.js", mode: "client" },
    { src: "~/plugins/vue-markdown.js", mode: "client" },
    { src: "~/plugins/vue-smooth-scrollbar", mode: "client" }
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  layoutTransition: {
    name: "layout",
    mode: "out-in"
  },

  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxtjs/style-resources"],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "@nuxtjs/style-resources",
    "@nuxtjs/proxy"
  ],
  proxy: {
    "/api/": {
      target: "https://api.interactivedesign.io",
      pathRewrite: {
        "^/api/": "/"
      },
      changeOrigin: true
    },
    "/send_data/": {
      target:
        "https://api.telegram.org/bot949493360:AAFxCnB4Cky_-LEkGMu1gbzEl2zc8zOcL34/sendMessage?chat_id=-426794697&parse_mode=html&text=",
      pathRewrite: {
        "^/send_data/": ""
      },
      changeOrigin: true
    },
    "/user_location/": {
      target: "https://ipinfo.io/?token=8bd4b15ef11f54",
      pathRewrite: {
        "^/user_location/": ""
      },
      changeOrigin: true
    }
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    // proxy: true,
    withCredentials: true
    // credentials: true,
    // debug: true,
    // headers: {
    //   common: {
    //     'Accept': 'application/json',
    //     'Referer': 'https://interactivedesign.io/'
    //   }
    // }
  },

  // serverMiddleware: [
  //   '~/api/index.js',
  // ],
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    styleResources: {
      scss: "./assets/scss/_nuxt-imports.scss"
    }
  }
};
